import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/auth";
import { LoaderIcon } from "../loader";
import "./LoginForm.css";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

export const LoginForm = ({ login }) => {
  const { loading, error } = useSelector((state) => ({
    loading: state.auth.loading,
    error: state.auth.error,
  }));

  const dispatch = useDispatch();

  const [state, setState] = useState({
    username: "",
    password: "",
  });

  const handleLogin = (event) => {
    event.preventDefault();
    dispatch(actions.login(state));
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <div id='login-page'>
      <Form id="login-form" onSubmit={handleLogin}>
        <h2>Login to Kwitter!</h2>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Kwitter username:</Form.Label>
          <br />
          <Form.Control
            type="username"
            name='username'
            placeholder="Enter your username"
            value={state.username}
            autoFocus
            required
            onChange={(e) => handleChange(e)}
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password:</Form.Label>
          <br />
          <Form.Control
            type="password"
            name='password'
            placeholder="Password"
            value={state.password}
            required
            onChange={(e) => handleChange(e)}
          />
        </Form.Group>
        <Button variant="primary" type="submit" disabled={loading}>
          Login
        </Button>
      

        <Button><Link id="createAccount" to="/addUser">
          Create New Account
        </Link>
        </Button>
      </Form>
      {loading && <LoaderIcon />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </div>
  );

};
