import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import {
  HomeScreen,
  ProfileScreen,
  NotFoundScreen,
  AddUserPage,
  MessagesPage,
  DisplayUsersPage,
  ViewProfileScreen,
} from "../../screens";
import { ConnectedRoute } from "../connected-route/ConnectedRoute";

export const Navigation = () => (
  <BrowserRouter>
    <Switch>
      <ConnectedRoute
        exact
        path="/"
        redirectIfAuthenticated
        component={HomeScreen}
      />
      <ConnectedRoute exact path="/messages" component={MessagesPage} />
      <ConnectedRoute
        exact
        isProtected
        path="/profiles/:username"
        component={ProfileScreen}
      />
      <ConnectedRoute exact path="/addUser" component={AddUserPage} />
      <ConnectedRoute exact path="/getUsers" component={DisplayUsersPage} />
      <ConnectedRoute
        exact
        isProtected
        path="/profile"
        component={ViewProfileScreen}
      />
      <ConnectedRoute path="*" component={NotFoundScreen} />
    </Switch>
  </BrowserRouter>
);
