import React, { useEffect, useState } from "react";
import "./Post.css";
import Logo from "../../logo.png";
import { useDispatch, useSelector } from "react-redux";
import { actions as likesActions } from "../../redux/actions/likes";
import { actions as messageActions } from "../../redux/actions/messages";
import { LikeButton } from "../likeButton/LikeButton";


const Post = ({ text, username, createdAt, likes, messageId }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.auth.username);
  const timestamp = new Date(createdAt).toDateString();
  const [likeId, setLikeId] = useState(null)
  const [liked,setliked]=useState(false)

  useEffect(() => {
    for (let like of likes) {
      
      if (like.username === currentUser) {
        setLikeId(like.id);
        setliked(true);
       }
    }
  }, [likes]);


  const handleLike = (event) => {
    event.preventDefault();
    if (liked) {
      dispatch(likesActions.removeLike(likeId));
    } else {
      dispatch(likesActions.addLike(messageId));
    }
  };

  const handleDelete = (e) => {
    e.preventDefault();
    dispatch(messageActions.deleteMessage(messageId));
  };

  return (
    <div id="post-container">
      <img id="post-profile-photo" src={Logo} alt="Profile Photo" />
      <div id="post-content">
        <div id="post-header">
          <h4 id="name">@{username}</h4>
        </div>
        <hr />
        <p>{text}</p>
        <footer id="post-footer">
          <p>
          <LikeButton 
            likes={likes}
            messageId={messageId}
            handleLike={handleLike}
          />
            {username === currentUser ? (
              <button
                className='delete'
                onClick={(e) =>
                  window.confirm(`Are you sure you want to delete your message?`)
                    ? handleDelete(e)
                    : null
                }
              >
                Delete Message
              </button>
            ) : null}
            {timestamp}
          </p>
        </footer>
      </div>
    </div>
  );
};

export default Post;
