import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/auth";
import "./Menu.css";
import Logo from "../../logo.png";

export const Menu = () => {
  const isAuthenticated = useSelector((state) => !!state.auth.isAuthenticated);
  const dispatch = useDispatch();
  const logout = () => dispatch(actions.logout());
  let root = document.documentElement;
  const updateMenuColor = () => {
    if (isAuthenticated) {
      root.style.setProperty('--menu-color', 'lightskyblue')
    } else {
      root.style.setProperty('--menu-color', 'rgb(180,180,180)')
    }
  }
  useEffect(() => {
    updateMenuColor()
  },[isAuthenticated])
  return (
    <div
      id="menu"
      className='componentHeader'
    >
      <h1>Music Box</h1>
      <div id="menu-links">
        {isAuthenticated ? (
          <>
            <Link to="/messages">Message Feed</Link>
            <Link to="/profiles/:username">Edit Profile</Link>
            <Link to="/profile">View Profile</Link>
            <Link to="/getUsers">Display Users</Link>
            <Link to="/" onClick={logout}>
              Logout
            </Link>
          </>
        ) : <h3>Your favorite music genre retreivial platform</h3>
      }
      </div>
      <img
        src={Logo}
        alt="Music Box Logo"
        width="70px"
        height="70px"
        style={{
          margin: "1rem",
          borderRadius: '50%',
          backgroundColor: 'rgba(250,250,250,.75)',
          padding: '2px'
        }}
      />
    </div>
  );
};
