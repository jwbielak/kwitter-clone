import React, { useState, useEffect } from "react";
import { LoaderIcon, MenuContainer } from "../components";
import "./Profile.css";
import { actions } from "../redux/actions/users";
import { useDispatch, useSelector } from "react-redux";
import { ViewProfileScreen } from "./ViewProfile";
import { Link } from "react-router-dom";


export const ProfileScreen = () => {
  const dispatch = useDispatch();
  const username = useSelector((state) => state.auth.username);
  const [displayName, setDisplayName] = useState("");
  const [password, setPassword] = useState("");
  const [Bio, setBio] = useState("");
  const loading = useSelector((state) => state.user.currentUser.loading);
  const currentUser = useSelector((state) => state.auth.username);
  let user = useSelector((state) => state.user.currentUser.user);
  useEffect(() => {
    dispatch(actions.getCurrentUser(currentUser));
  }, []);
  const changeDisplayName = (e) => {
    e.preventDefault();
    dispatch(actions.updateProfile(username, { displayName }));
    setDisplayName("");
  };
  const changeBio = (e) => {
    e.preventDefault();
    dispatch(actions.updateProfile(username, { about: Bio }));
    setBio("");
  };
  const changePassword = (e) => {
    e.preventDefault();
    dispatch(actions.updateProfile(username, { password }));
    setPassword("");
  };
  const handleDelete = (e) => {
    e.preventDefault();
    console.log(username);
    dispatch(actions.deleteUser(username));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (Bio) {
      changeBio(e);
    }
    if (password) {
      changePassword(e);
    }
    if (displayName) {
      changeDisplayName(e);
    }
    alert(`Information updated! Tap the 'View Profile' link above to see your new profile.`)
  };

  return (
    <div id="editProfilePage">
      <MenuContainer />
      <div id="editProfileContainer">
        <header className="componentHeader">
          <h1 style={{color: 'var(--primary-blue'}}> Edit Your Profile </h1>
        </header>
        <hr />
        <form id="updateProfile" onSubmit={(e) => handleSubmit(e)}>
          <label>
            New Nickname:
            <br/>
            <input
              type="text"
              name="changeDisplayName"
              placeholder="Enter new display name"
              value={displayName}
              onChange={(e) => {
                console.log(displayName);
                setDisplayName(e.target.value);
              }}
            />
          </label>
          <br/>
          <label>
            New Password:
            <br/>
            <input
              type="password"
              name="changePassword"
              value={password}
              placeholder="Enter new password"
              onChange={(e) => {
                console.log(`new password: ${password}`);
                setPassword(e.target.value);
              }}
            />
          </label>
          <br/>
            <label>
              Choose your music genre:
              <br/>
              <input
                style={{
                  width: "21rem",
                }}
                type="text"
                name="Biography"
                placeholder="Please tell us your favorite music genre"
                value={Bio}
                onChange={(e) => {
                  setBio(e.target.value);
                }}
              />
            </label>
            <br/>
            <button type="submit">Submit Changes</button>
        </form>
        <hr />
        <button
          id="deleteUserButton"
          className="delete"
          onClick={(e) =>
            window.confirm(
              `Are you sure you want to delete your account? We're pretty cool!`
            )
              ? handleDelete(e)
              : null
          }
        >
          Delete Account
        </button>
      </div>
      {loading && <LoaderIcon />}
    </div>
  );
};
