import React from "react";
import { LoginFormContainer, MenuContainer } from "../components";

export const HomeScreen = () => (
  <>
    <MenuContainer />
    <LoginFormContainer />
  </>
);
