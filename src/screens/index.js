import { format } from "path";

export * from "./Home";
export * from "./Profile";
export * from "./NotFound";
export * from "./AddUser";
export * from './ViewProfile'
export * from "./Messages";
export * from "./displayUsers";

export { MessagesPage }
from "./Messages";