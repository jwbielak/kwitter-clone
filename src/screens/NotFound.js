import React from "react";
import { Link } from "react-router-dom";
import milkandcat from "../components/Images/404 music.png"
import { MenuContainer } from "../components";

const NotFound = ({ location }) => (
  <>
  <MenuContainer/>
  <div id='notFoundPage'>
  <h1>Oh, no!</h1>
    <p>It looks like team milkcrate ran out of funding to create  {location.pathname}.
    We are so sorry about this but,
    Team Music Box works on a limited budget to bring you the best Music Box experience. </p>

  <img src={milkandcat} alt="Logo" width='350px' height='350px' />
  <br></br>
    <button><Link to="/" className='linkButton'>Go back to Homepage</Link></button>
    <br></br>
    {/* <Link to="/Kano">Speak with Team MilkCrate's supervisor Kano to help provide additional funding</Link> */}
    </div>
  </>
);

export const NotFoundScreen = NotFound;
