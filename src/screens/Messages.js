import React, { useState } from "react";
import Post from "../components/post/Post";
import { MenuContainer } from "../components/menu";
import { useEffect } from "react";
import { actions } from "../redux/actions/messages";
import { useDispatch, useSelector } from "react-redux";
import { LoaderIcon } from "../components/loader";

export const MessagesPage = () => {
  const dispatch = useDispatch();
  const messages = useSelector((state) => state.messages.getMessages.messages);
  // const [messagesToDisplay,setMessagesToDisplay]=useState(messages.slice())
const[sorted,setSorted]=useState(false)
  const loading = useSelector((state) => state.messages.getMessages.loading);
  const [newMessage, setNewMessage] = useState('')

  const exampleMessage = {
    id: 0,
    text: "sample message text",
    username: "drewskiiiiiiiii",
    createdAt: "2020-09-07T14:35:45.564Z",
    likes: [
      {
        id: 0,
        username: "string",
        messageId: 0,
        createdAt: "2020-09-07T14:35:45.564Z",
      },
    ],
  };
 
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(`posting message: ${newMessage}`);
    let newPost = {text: newMessage}
    dispatch(actions.postMessage(newPost))
    setNewMessage('')
  }
  useEffect(() => {
    dispatch(actions.getMessages());
    // setMessagesToDisplay(messages)

  }, []);
// const sortbylikes=(e)=>{
// e.preventDefault()
// let likesArray = []
// let sortedMessages= messages.sort(function(a,b){return b.likes.length-a.likes.length})
//   sortedMessages.map((message,i)=>{
//     if (i<10){
//       likesArray.push(message)
//     }
//   }
//   )
// console.log(likesArray)
// setMessagesToDisplay(likesArray)
// }
  return (
    <>
      <MenuContainer />
      <div id='messagesDiv'>
      <form
        id='postMessageForm'
        onSubmit={(e) => handleSubmit(e)}
      >
          <input
            type="text"
            value={newMessage}
            name="post"
            onChange={(e) => {
              setNewMessage(e.target.value)
              console.log(newMessage)
            }}
            id='postMessageInput'
            autoFocus
            placeholder='Tell us your deepest thoughts... (or anything, really)'
          />
        <button type="submit" >Post Kweet!</button>
      </form>        <button onClick={e=>{
          e.preventDefault()
          setSorted(!sorted)
          dispatch(actions.getMessages());
        }}>{sorted ? 'Sort Kweets Chronologically' : 'Sort Kweets by Popularity'}</button>

      {sorted ?
            messages.sort(function(a,b){return b.likes.length-a.likes.length}).map((message) => (
        <Post
          text={message.text}
          username={message.username}
          createdAt={message.createdAt}
          likes={message.likes}
          messageId={message.id}
          key={message.id}
        />
      ))
             : messages && messages.map((message) => (
        <Post
          text={message.text}
          username={message.username}
          createdAt={message.createdAt}
          likes={message.likes}
          messageId={message.id}
          key={message.id}
        />
      ))}
      {loading && <LoaderIcon />}
      </div>
    </>
  );
};
export default MessagesPage;