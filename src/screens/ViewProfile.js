import React, { useEffect } from "react";
import { MenuContainer } from "../components";
import { actions } from "../redux/actions/users";
import { useDispatch, useSelector } from "react-redux";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Logo from "../logo.png";
import {Link} from 'react-router-dom';

export const ViewProfileScreen = () => {
  const dispatch = useDispatch();
  const username = useSelector((state) => state.auth.username);
  // const user = dispatch(actions.getCurrentUser(username));

  let user = useSelector((state) => state.user.currentUser.user);

  useEffect(() => {
    dispatch(actions.getCurrentUser(username));
  }, []);

  return (
    <>
      <MenuContainer />
      <Card id="profileCard" >
        <Card.Img
          variant="top"
          src={Logo}
          height="150px"
          width="150px"
        />
        <Card.Body>
          <Card.Title>
            <h1 style={{color: 'var(--primary-blue)'}}>@{username}</h1>
          </Card.Title>
          <Card.Text>
            <h2 style={{color: 'black'}}>Nickname: {user.displayName}</h2>
            <h3>Genre:</h3>
            <p>{user.about ? user.about : `Click 'Edit Profile' to tell others about you!`}</p>
          </Card.Text>
          <Button><Link id='editProfileLink' to='/profiles/:username'>Edit Profile</Link></Button>
        </Card.Body>
      </Card>
    </>
  );
};
