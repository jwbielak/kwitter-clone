// this is all from Kevin's code for likes

import {
    ADD_LIKE,
    ADD_LIKE_FAILURE,
    ADD_LIKE_SUCCESS,
    REMOVE_LIKE,
    REMOVE_LIKE_SUCCESS,
    REMOVE_LIKE_FAILURE,
} from "../actions";

const INITIAL_STATE = {
    getLikes: {
        likes: [],
        loading: false,
        error: "",
    },
};
export const likesReducer = (state = {...INITIAL_STATE }, action) => {
    switch (action.type) {
        case ADD_LIKE:
            return {
                ...INITIAL_STATE,
                getLikes: {
                    ...INITIAL_STATE.getLikes,
                    loading: true,
                },
            };
        case ADD_LIKE_SUCCESS:
            return {
                ...INITIAL_STATE,
                getLikes: {
                    ...INITIAL_STATE.getLikes,
                    loading: false,
                    likes: action.payload,
                },
            };
        case ADD_LIKE_FAILURE:
            return {
                ...INITIAL_STATE,
                getLikes: {
                    ...INITIAL_STATE,
                    loading: false,
                    error: action.payload,
                },
            };
        case REMOVE_LIKE:
            return {
                ...INITIAL_STATE,
                getLikes: {
                    ...INITIAL_STATE.getLikes,
                    loading: true
                }
            }
        case REMOVE_LIKE_SUCCESS:
            return {
                ...INITIAL_STATE,
                getLikes: {
                    ...INITIAL_STATE.getLikes,
                    loading: false,
                    likes: action.payload,
                },
            };
        case REMOVE_LIKE_FAILURE:
            return {
                ...INITIAL_STATE,
                getLikes: {
                    ...INITIAL_STATE.getLikes,
                    loading: false,
                    error: action.payload,
                },
            };
        default:
            return state;
    }
};