import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
    isAuthenticated: "",
    username: "",
    loading: false,
    error: "",
};

export const authReducer = (state = {...INITIAL_STATE }, action) => {
    switch (action.type) {
        case LOGIN:
            console.log(state)
            return {
                ...INITIAL_STATE,
                loading: true,
            };
        case LOGIN_SUCCESS:
            const { username, token } = action.payload;
            return {
                ...INITIAL_STATE,
                isAuthenticated: token,
                username,
                loading: false,
            };
        case LOGIN_FAILURE:
            alert(`Hmm, that didn't work. Check your username and password and try again.`)
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false,
            };
        case LOGOUT:
            return {
                ...INITIAL_STATE,
            };
        default:
            return state;
    }
};