// TODO: implement

import {
    ADD_USER,
    ADD_USER_SUCCESS,
    ADD_USER_FAILURE,
    GET_USERS,
    GET_USERS_SUCCESS,
    GET_USERS_FAILURE,
    DELETE_USER,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE,
    GET_CURRENT_USER,
    GET_CURRENT_USER_SUCCESS,
    GET_CURRENT_USER_FAILURE,
    UPDATE_USER,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE,

} from "../actions";

const INITIAL_STATE = {
    getUsers: {
        users: [],
        loading: false,
        error: "",
    },
    addUser: {
        loading: false,
        error: "",
        success: "",
    },
    deleteUser: {
        loading: false,
        error: "",
    },
    currentUser: {
        loading: false,
        user: {},
        error: "",
    },
    updateProfile: {
        loading: false,
        error: "",
        update: {},
    },
};

export const userReducer = (state = {...INITIAL_STATE }, action) => {
    switch (action.type) {
        case GET_USERS:
                return {
                ...INITIAL_STATE,
                getUsers: {
                    ...INITIAL_STATE.getUsers,
                    loading: true,
                },
            };
        case GET_USERS_SUCCESS:
            const { users } = action.payload;
            return {
                ...INITIAL_STATE,
                getUsers: {
                    ...INITIAL_STATE.getUsers,
                    loading: false,
                    users: users,
                },
            };
        case GET_USERS_FAILURE:
            return {
                ...INITIAL_STATE,
                getUsers: {
                    ...INITIAL_STATE.getUsers,
                    loading: false,
                    error: action.payload,
                },
            };
        case ADD_USER:
            return {
                ...INITIAL_STATE,
                addUser: {
                    ...INITIAL_STATE.addUser,
                    loading: true,
                },
            };
        case ADD_USER_SUCCESS:
            alert('Congratulations. You have registered successfully!')
            return {
                ...INITIAL_STATE,
                addUser: {
                    ...INITIAL_STATE.addUser,
                    loading: false,
                    success: action.payload,
                },
            };
        case ADD_USER_FAILURE:
            alert('Looks like the username is already taken. Please try again...' )
            return {
                ...INITIAL_STATE,
                addUser: {
                    ...INITIAL_STATE.addUser,
                    loading: false,
                    error: action.payload,
                },
            };
        case DELETE_USER:
            return {
                ...INITIAL_STATE,
                deleteUser: {
                    ...INITIAL_STATE.deleteUser,
                    loading: true,
                },
            };
        case DELETE_USER_SUCCESS:
            return {
                ...INITIAL_STATE,
                deleteUser: {
                    ...INITIAL_STATE.deleteUser,
                    loading: false,
                },
            };
        case DELETE_USER_FAILURE:
            return {
                ...INITIAL_STATE,
                deleteUser: {
                    ...INITIAL_STATE.deleteUser,
                    loading: false,
                },
            };
        case GET_CURRENT_USER:
            return {
                ...INITIAL_STATE,
                currentUser: {
                    ...INITIAL_STATE.currentUser,
                    loading: true,
                },
            };
        case GET_CURRENT_USER_SUCCESS:
            return {
                ...INITIAL_STATE,
                currentUser: {
                    ...INITIAL_STATE.currentUser,
                    loading: false,
                    user: action.payload.user,
                },
            };
        case GET_CURRENT_USER_FAILURE:
            return {
                ...INITIAL_STATE,
                currentUser: {
                    ...INITIAL_STATE.currentUser,
                    loading: false,
                    error: action.payload,
                },
            };
        case UPDATE_USER:
            return {
                ...INITIAL_STATE,
                updateProfile: {
                    ...INITIAL_STATE.updateProfile,
                    loading: true,
                }
            };
        case UPDATE_USER_SUCCESS:
            return {
                ...INITIAL_STATE,
                updateProfile: {
                    ...INITIAL_STATE.updateProfile,
                    loading: false,
                    update: action.payload,
                }
            };
        case UPDATE_USER_FAILURE:
            return {
                ...INITIAL_STATE,
                updateProfile: {
                    ...INITIAL_STATE.updateProfile,
                    loading: false,
                    error: action.payload
                }
            };
        default:
            return state;
    }
};