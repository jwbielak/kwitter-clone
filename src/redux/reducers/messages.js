// TODO: implement
import {
    GET_MESSAGES,
    GET_MESSAGES_SUCCESS,
    GET_MESSAGES_FAILURE,
    POST_MESSAGE,
    POST_MESSAGE_SUCCESS,
    POST_MESSAGE_FAILURE
} from "../actions";

const INITIAL_STATE = {
    getMessages: {
        messages: [],
        loading: false,
        error: "",
    },
    getLikes: {
        likes: [],
        loading: false,
        error: "",
    }
};



export const messagesReducer = (state = {...INITIAL_STATE }, action) => {
    switch (action.type) {
        case GET_MESSAGES:
            return {
                ...INITIAL_STATE,
                getMessages: {
                    ...INITIAL_STATE.getMessages,
                    loading: true,
                },
            };
        case GET_MESSAGES_SUCCESS:
            const { messages } = action.payload;
            return {
                ...INITIAL_STATE,
                getMessages: {
                    messages: messages,
                    loading: false,
                },
            };
        case GET_MESSAGES_FAILURE:
            return {
                ...INITIAL_STATE,
                getMessages: {
                    ...INITIAL_STATE.getMessages,
                    loading: false,
                    error: action.payload,
                },
            };
        case POST_MESSAGE:
            return {
                ...INITIAL_STATE,
                postMessage: {
                    ...INITIAL_STATE.postMessage,
                    loading: true
                },
            };
        case POST_MESSAGE_SUCCESS:
            return {
                ...INITIAL_STATE,
                postMessage: {
                    ...INITIAL_STATE.postMessage,
                    loading: false,
                    message: action.payload
                },
            };
        case POST_MESSAGE_FAILURE:
            return {
                ...INITIAL_STATE,
                postMessage: {
                    ...INITIAL_STATE.postMessage,
                    loading: false,
                    error: action.payload
                }
            }
        default:
            return state;
    }
};