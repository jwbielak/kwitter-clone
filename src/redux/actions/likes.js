// this is all from Kevin's code for likes
import api from "../../utils/api"
import { actions as messagesActions } from './messages'

export const ADD_LIKE = 'ADD_LIKE'
export const ADD_LIKE_SUCCESS = 'ADD_LIKE_SUCCESS'
export const ADD_LIKE_FAILURE = 'ADD_LIKE_FAILURE'
export const REMOVE_LIKE = 'REMOVE_LIKE'
export const REMOVE_LIKE_SUCCESS = 'REMOVE_LIKE_SUCCESS'
export const REMOVE_LIKE_FAILURE = 'REMOVE_LIKE_FAILURE'

const addLike = (messageId) => async(dispatch, getState) => {
    try {
        // const username = getState().auth.username
        // const messages = getState().messages.getMessages.messages
        // console.log(messageId)
        dispatch({ type: ADD_LIKE })
        const payload = await api.ADD_LIKE({ messageId })
        dispatch({ type: ADD_LIKE_SUCCESS, payload })
        dispatch(messagesActions.getMessages())
    } catch (err) {
        console.log('like button failed');
        dispatch({ type: ADD_LIKE_FAILURE, payload: err.message });
    }
};

const removeLike = (likeId) => async(dispatch, getState) => {
    try {
        // console.log(`removing like initiated`)
        dispatch({ type: REMOVE_LIKE })
        const payload = await api.REMOVE_LIKE(likeId)
        dispatch({ type: REMOVE_LIKE_SUCCESS, payload })
        dispatch(messagesActions.getMessages())
    } catch (err) {
        // console.log('removing like failed')
        dispatch({ type: REMOVE_LIKE_FAILURE, payload: err.message })
    }
}


export const actions = {
    addLike,
    removeLike
};