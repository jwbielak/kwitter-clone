import api from "../../utils/api";

export const GET_MESSAGES = 'GET_MESSAGES'
export const GET_MESSAGES_SUCCESS = 'GET_MESSAGES_SUCCESS'
export const GET_MESSAGES_FAILURE = 'GET_MESSAGES_FAILURE'
export const POST_MESSAGE = 'POST_MESSAGE'
export const POST_MESSAGE_SUCCESS = 'POST_MESSAGE_SUCCESS'
export const POST_MESSAGE_FAILURE = 'POST_MESSAGE_FAILURE'
export const DELETE_MESSAGE = 'DELETE_MESSAGE'
export const DELETE_MESSAGE_SUCCESS = 'DELETE_MESSAGE_SUCCESS'
export const DELETE_MESSAGE_FAILURE = 'DELETE_MESSAGE_FAILURE'


const getMessages = () => async(dispatch, getState) => {
    //  console.log('actions/users getUser action')
    try {
        dispatch({ type: GET_MESSAGES })
            // We do not care about the result of logging out
            // as long as it succeeds
        const payload = await api.getMessages();
        dispatch({ type: GET_MESSAGES_SUCCESS, payload })
    } catch (err) {
        /**
         * Let the reducer know that we are logged out
         */
        console.log('getMessages failed');
        dispatch({ type: GET_MESSAGES_FAILURE, payload: err.message });
    }
};

const _postMessage = (text) => async(dispatch, getState) => {
    try {
        dispatch({ type: POST_MESSAGE })
        const payload = await api.postMessage(text);
        dispatch({ type: POST_MESSAGE_SUCCESS, payload })
    } catch (err) {
        console.log("post message failed");
        dispatch({ type: POST_MESSAGE_FAILURE, payload: err.message })
    }
}
const postMessage = (text) => async(dispatch, getState) => {
    console.log('post message attempt initiated')
    return dispatch(_postMessage(text))
        .then(() => { return dispatch(getMessages()) })
}

const _deleteMessage = (messageId) => async(dispatch, getState) => {
    try {
        dispatch({ type: DELETE_MESSAGE })
        const payload = await api.deleteMessage(messageId);
        dispatch({ type: DELETE_MESSAGE_SUCCESS, payload })
    } catch (err) {
        console.log('delete message failed');
        dispatch({ type: DELETE_MESSAGE_FAILURE, payload: err.message })
    }
}

const deleteMessage = (messageId) => async(dispatch, getState) => {
    console.log('delete message initiated')
    return dispatch(_deleteMessage(messageId))
        .then(() => { return dispatch(getMessages()) })
}

export const actions = {
    getMessages,
    postMessage,
    deleteMessage
};